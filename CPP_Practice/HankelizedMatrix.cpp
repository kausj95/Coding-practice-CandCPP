#include "iostream";
using namespace std;

const int M = 3;    //no of rows
const int N = 6;    //no of columns

bool hankelizeMatrix(double A[][N], int M, int N)
{
    if(M >= N || M < 0 || N < 0)
    {
        return false;    
    }
    
//access the matrix diagonally till M=N

    for(int k=0; k<M; k++)
    {
        int i = k;
        int j = 0;
        int count = 0;
        int temp = 0;
        
        while(i>=0) //add diagonal elements in each diagonal 
        {
            temp += A[i][j];
            i = i-1;
            j = j+1;
            count++;    //maintain count of elements in the diagonal
        }
        
        double avg = temp/count;    //find average
        // printf("%lf \n",avg);
        
        count = 0;
        temp = 0;
        
        i = k;
        j = 0;
        while(i>=0)
        {
            A[i][j] = avg;
            i = i-1;
            j = j+1;
        }
    }

// access the elements diagonally for remaining matrix.

    for(int k=1; k<N; k++)
    {
        int i = M-1;
        int j = k;
        int temp = 0;
        int count = 0;
        while(j < N && i >= 0)
        {
            temp += A[i][j]; 
            
            i = i-1;
            j = j+1;
            
            count++;
        }
        
        double avg = temp/count;
        
        //printf("%lf ",avg);
        //printf("\n");
        
        count = 0;
        temp = 0;
        
        i = M-1;
        j = k;
        while(j < N)
        {
            A[i][j] = avg;
            
            i = i-1;
            j = j+1;
        }
    }

    for(int i = 0; i<M; i++)    //print hankelized matrix
    {
        for(int j = 0; j<N; j++)
        {
            printf("%f ",A[i][j]);
        }
        printf("\n");
    }
    
    return true;

}

int main()
{
    double A[3][6] = {{2, 5, -1, 9, -3, 6}, {7, 4, 2, 4, 2, 10}, {3, -2, -10, 1, 4, 2}};
    int M = 3;
    int N = 6;
    // Write code below that uses M, N and A as input so that it will work
    // for any M X N matrix (M &lt; N). Verify output is correct for the initialized A matrix
    // Feel free to write additional functions
    
    bool result = hankelizeMatrix(A, M, N);
    
    if(result)
    {
        printf("Success");
    }
    else
    {
        printf("Invalid inputs");
    }
    
    return 0;
}