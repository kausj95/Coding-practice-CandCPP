class Solution {
public:
    bool isPalindrome(int x)
    {
        if(x<0)
            return 0;
        else
        {
            string str = to_string(x);
            int len=str.length();
            int y=0;
            if(len%2 == 0)
            {
                for(int i = 0; i<len/2; i++)
                {
                    if(str[i] == str[len-1-i])
                    {
                        y++;
                    }
                }
                if(y == len/2)
                    return 1;
                else
                    return 0;
            }
            else 
            {
                for(int i = 0; i<=len/2; i++)
                {
                    if(str[i] == str[len-1-i])
                        y++;
                }  
                if(y == ((len+1)/2))
                   return 1;    
                else
                   return 0;  
            }
        }
    }         
};
