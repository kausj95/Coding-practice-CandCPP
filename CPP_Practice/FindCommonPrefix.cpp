class Solution 
{
public:
    string longestCommonPrefix(vector<string>& strs) 
    {
        
        string res = "";
        string temp = "";
        
        if(strs.empty())
        {
            res = "";
        }
        
        else
        {
            res = strs[0];
            
            if(strs.size() == 1)
            {
                return res;
            }
            
            for(int i=0;i<(strs.size()-1);i++)
            {   
                for(int j=0;j<res.size() && j<(strs[i+1].size());j++)
                {
                    if(res[j] != strs[i+1][j])
                    {
                        break;
                    }
                    else
                    {
                        temp = temp+res[j];
                    }
                }
               res = temp; 
               temp="";
            }
        }
    return res;
    }

};