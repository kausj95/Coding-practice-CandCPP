class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        std::map<int, int>m1;
        int x, y, z;
        for(int i=0; i<nums.size(); i++)
        {
             x = target - nums[i];
            if(m1.find(x) != m1.end())
            {
                y = i;
                z = m1[x]; 
                vector<int> vec =  { z, y };
                return vec;
            }
             m1[nums[i]] = i;
        }
    }
};
