#include <iostream>

using namespace std;

int main()
{
    cout<<"Dynamic Programming";
    
    // 0 , 1 , 1 , 2 , 3 ..., n - 2 , n - 1 ,  n
    
    int base_0 = 0;
    int base_1 = 1;
    int ith = 0;
    
    int n = 15;
    
    for (int i = 2; i < n + 1; i++) {
        
        ith = base_1 + base_0;
        base_0 = base_1;
        base_1 = ith;

    }
    
    cout<< "\n" << ith;
    
    return 0;
}
