#include <iostream>
using namespace std;
 
char *removeDuplicate(char str[], int n)
{
   int j,c = 0;   
   for (int i=0; i<n; i++) 
   { 
     for (j=0; j<n; j++) 
     {
         if (str[i] == str[j])
         {
             if(i == j)
             {
                str[c] = str[i];
                c++;
             }
            break;  
         }
      }
   }
   return str;
}
 
// Driver code
int main()
{
   char str[]= "Geeksforgeeks";
   int n = sizeof(str) / sizeof(str[0]);
   cout << removeDuplicate(str, n);
   return 0;
}
