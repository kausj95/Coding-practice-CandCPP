#include <iostream>
#include<string>

using namespace std;

string concat(string s1, string s2)
{
    string s3 = "";
        
    for(int i=0; i<s1.length()||i<s2.length(); i++)
    {
        if(i < s1.length())
            s3 = s3+s1[i];
            
        if(i < s2.length())
            s3 = s3+s2[i];
    }
    
    return s3;
}


int main()
{
    string s1 = "abc";
    string s2 = "defghi";
    
    string res = concat(s1,s2);
    
    cout<<res;

    return 0;
}