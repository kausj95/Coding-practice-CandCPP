class Solution {
public:
    int romanToInt(string s) {
        int result = 0;
        
        for(int i=0;i<s.size();i++)
        {
            switch(s[i])
            {
                case 'I': 
                    if(s[i+1] == 'V' || s[i+1] == 'X')
                    {
                       result = result-1; 
                    }
                    else
                    {
                        result+=1;
                    }
                    break;
                case 'V': result+=5;
                    break;
                case 'X': 
                    if(s[i+1] == 'L' || s[i+1] == 'C')
                    {
                        result=result-10;
                    }
                    else
                    {
                        result+=10;
                    }
                    break;
                case 'L': result+=50;
                    break;
                case 'C': 
                    if(s[i+1] == 'D' || s[i+1] == 'M')
                    {
                        result=result-100;
                    }
                    else
                    {
                        result = result+100;
                    }
                    break;
                case 'D': result+=500;
                    break;
                case 'M': result+=1000;
                    break;
                default : result+=0;
            }  
        }
        return result;
    }
};