#include <iostream>
#include <string>

void revStr()
{
    std::string s = "Hello World";
    int n = s.size();
    int mid = (n)/2;
    for(int i=0; i<mid; i++)
    {
        int temp = s[i];
        s[i] = s[n-i-1];
        s[n-i-1] = temp;
    }
     std::cout<<s<<std::endl;
}

int main()
{
    revStr();
    return 0;
}
