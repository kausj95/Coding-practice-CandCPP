#include <iostream>

char* revStr(char a[])
{
    int n = sizeof(a);
    std::cout << n << std::endl;
    int mid = n/2;
    int last = n-1;
    for(int i=0; i<mid; i++)
    {
        int temp = a[i];
        a[i] = a[last-i];
        a[last-i] = temp;
    }
    return a;
}

int main()
{
    char a[] = "Chandu";
    char* s = revStr(a);
    for(int i=0;i<(sizeof(s)/sizeof(s[0]));i++)
    {
        std::cout << s[i];
    }
    return 0;
}
