class Solution {
public:
    int singleNumber(vector<int>& nums) {
        
        map<int,int> Dups;
        int sol;
        
        for(int i=0; i<nums.size();i++)
        {
            int count=1;
            if(Dups.count(nums[i]))
            {
                Dups.erase(nums[i]);
            }
            
            else
            {
                Dups.insert(pair<int,int>(nums[i],count));
                cout<<nums[i]<<endl;
            }
        }
        map<int,int>::iterator it;
        for(it=Dups.begin();it!=Dups.end();it++)
            sol = it->first;
        
        return sol;
    }
};