// Using the given function prototypes, implement a solution
// for the UART consumer problem described below.
//
// Your program should read data from the UART in the UART
// ISR (uart_rx_isr) and pass this to the the consumer task.
//
// After reading a full data_t structure into memory, the
// application should invoke consume_data()
//
// A correct solution must define an appropriate definition
// for data_t, and must not waste time in the application
// task when data is not available from the UART.
//
// The data format on the wire for data_t is:
//  |integerA | intergerB   |
//	  2 bytes   4 bytes
//
// * You may assume that the compiler is C99 standard complient.
// * You are free to use any standard library function.
// * If you feel that you have insufficient information to
//   solve the problem, you are encouraged to ask questions.
// * You can assume that typical "pragma" type directives are
//   supported by the compiler and encouraged to use them...
//   provided that they lead to a more elegant or natural
//   solution.



// include all C standard library functions
#include "c_everything.h"


//
// Provided API
//

// acquire a semaphore
void semaphore_take(sem_t semaphore);


// release the semaphore
void semaphore_give(sem_t semaphore);


// create a binary semaphore
sem_t semaphore_create_binary(void);



// create a mutex semaphore
sem_t semaphore_create_mutex(void);


//
// All routines are thread-safe, non-blocking, and may be used from ISR context
//

// read up to len bytes of data from a global buffer and
// returns the number of bytes read
size_t queue_read(void* data, size_t len);


// write data into a global buffer
void queue_write(void* data, size_t len);


// a library routine that reads a byte from the UART
uint8_t uartlib_read_byte(void);


// enable the ISR
// NOTE: the ISR will not fire until this routine is invoked
void uartlib_rx_isr_enable(void);


// consumes the data (this should be invoked by the
// application task)
void consume_data(data_t* data);


///////////////////////////////////////////////////////////
//
// your program
//
///////////////////////////////////////////////////////////


///create a structure
typedef struct
{
    
    uint8_t TxArr[6];
    
}Tx_struct;


// you may modify main if you wish (but you needn't do so)
// os_start is the entry point of the OS.

//create a binary semaphore handle
sem_t binSem;

void main(void)
{
	os_task_create(uart_rx_task, UART_STACK_SIZE);
	os_start();
}


// implements an ISR that receives data from the uart
void uart_rx_isr(void)
{
	// implement me
	uint8_t data = uartlib_read_byte();
	queue_write(data,sizeof(uint8_t));
	semaphore_give(binSem);
	
}


// implements a task that reads in serialized data from the
// UART and passes it to consume_data()
void uart_rx_task(void)
{
    uint8_t count
    = 0;
	// implement me
	while(1)
	{
	    semaphore_take(binSem);
	    
        count += queue_read(data[count],(6-count));
    //read all 6 bytes together
        if(count >= 6)
        {
    
        //queue_read(data, 6);
            consume_data(data);
            count = 0;
        }
	}
}
