/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

struct ListNode* removeNthFromEnd(struct ListNode* head, int n){
    
        struct ListNode* curr = (struct ListNode*)malloc(sizeof(struct ListNode));
        struct ListNode* dummyHead = (struct ListNode*)malloc(sizeof(struct ListNode));
        
        curr = head; 
        dummyHead->next = head;
        int count = 0;
    
        while(curr->next != NULL)
        {
            curr = curr->next;
            count++;
        }
    
        if(count == 0)
            return head->next;
        
        count = count-n;
        curr = head;
        
        while(count>0)
        {
            count--;
            curr = curr->next;
        }
    
        curr->next = curr->next->next;
        
    return dummyHead->next;
}