/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */


int maxDepth(struct TreeNode* root){
    int depth = 0;
    if(root == NULL)
    {
        return depth;
    }
    
    else
    {
        int lDepth = maxDepth(root->left);
        int rDepth = maxDepth(root->right);
        
        if(lDepth>rDepth)
        {
            depth = lDepth+1;
        }
        else
        {
            depth = rDepth+1;
        }
    }

    return depth;
}
